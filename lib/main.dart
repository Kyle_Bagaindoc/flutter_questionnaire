import 'package:flutter/material.dart';
import 'package:flutter_questionnaire/body_answers.dart';
import'/body_questions.dart';

void main(){
    //The App() is the root widget.
    runApp(App());

}   

class App extends StatefulWidget {
  @override
  AppState createState() => AppState();


  }


class AppState extends State<App> {
    int questionIdx = 0;
    bool showAnswers = false;

     final questions = [
         {
            'question': 'What is the nature of your business?',
            'options': [ 'Time Tracking', 'Asset Management','Issue Tracking']
         },
                          
         {
            'question': 'What is the expected size of the user base?',
            'options': [   'Less than 1000', 'Less Than 10,000', 'More Than 1,000']
         },
         {
            'question': 'In which region would the majority of the user base be?',
            'options': ['Asia','Europe', 'Americas', 'Africa', 'Middle East' ]
         },
         {
            'question': 'What is the expected project duration?',
            'options':  ['Less than 3 months', '3-6 months', '6-9 months', '9-12 months', 'More than 12 months']
         }];      
 
 var answers = [];


//questions[0] => answers[0]

    void nextQuestion(String? answer){
        answers.add({
            'question': questions[questionIdx]['question'],
            'answer': (answer == null )? '' :answer
        });

    if (questionIdx < questions.length - 1){
        setState(() => questionIdx++);{
          
        } 
        }
        else {
            setState(()=> showAnswers = true); 
        }
    
        // questionIdx++;
        print(questionIdx);
    }

    @override
    Widget build(BuildContext context) {
      var bodyQuestions = BodyQuestions(questions: questions, questionIdx: questionIdx, nextQuestion: nextQuestion);
    var bodyAnswers = BodyAnswers(answers: answers);

Scaffold homepage = Scaffold (
    appBar: AppBar(title: Text('Homepage')),
    body : (showAnswers)? bodyAnswers: bodyQuestions
    

);
   return MaterialApp(
       home: homepage
   );
  }

}



//The invisible widgets are Container and Scaffold.
//The visible widgets are AppBar and Text.
//To specify margin or padding spacing, we can use EdgeInsets.
//The values for spcaing are in multiples of 8 (e.g. 16, 24, 32).
//To add spacing on all sides use EdgeInsets.all().
//To add spacing on certain sides, use EdgeInsets.only(direction: value)      

//Double. Infinity is equivalent of width = 100% in CSS.
//To put colors on a container, the decoration //decoration: BoxDecoration(color: Colors.green),
//In a Column widget the main axis alignment is vertical (or from top to bottom).
//To change the horizontal alignment of widgets in a column, we must use the crossAxisAlignment.
//If we want to place the column widgets to the horizontal left we set crossAxisAlignment.start.
//In the context of the column we can consider the column as vertical and it's cross axis as horizontal. 
// mainAxisAlignment: MainAxisAlignment.center,
//Text can be put outside or inside.

   //The scaffold widget provides basic layout structure.
      //The scaffold can be given UI elements sush as an app bar.

/*
MaterialApp
    Scaffold
        AppBar
            Text
                Container
                    Text
*/
//State something that we use to manage data


//The state is lifted up
//App.questionIdx = 2
//-> Bodyquestions.questionIdx = 2

//Single Responsibility Principle